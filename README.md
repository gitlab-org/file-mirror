# Mirror for http://darwinsys.com/file

This is a simple package mirror for the unix `file` package.

## Adding a new release

Create a tag/release with the version number, e.g. `5.39`. The CI will fetch the archive from the FTP server and upload it to [the package registry](https://gitlab.com/gitlab-org/file-mirror/-/packages)
